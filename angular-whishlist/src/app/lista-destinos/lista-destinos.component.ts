import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { destinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {
@Output() onItemAdded: EventEmitter<destinoViaje>;
updates: string[];

  constructor(public destinosApiClient:DestinosApiClient){    
    this.onItemAdded = new EventEmitter();
    this.updates= [];
    this.destinosApiClient.subscribeOnChange((d: destinoViaje) => {
    	if(d != null){
    	this.updates.push('Se ha elegido a ' + d.nombre + ' como destino preferido.');
    	}
    });
  }

  ngOnInit(): void {}

  agregado(d: destinoViaje) {
  	this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: destinoViaje) {
    this.destinosApiClient.elegir(e);
    }
}
