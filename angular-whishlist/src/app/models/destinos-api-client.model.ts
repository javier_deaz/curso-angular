import { destinoViaje } from './destino-viaje.model';
import {Subject, BehaviorSubject} from 'rxjs';

export class DestinosApiClient {
  destinos:destinoViaje[];
  current: Subject<destinoViaje> = new BehaviorSubject<destinoViaje>(null); 
  constructor() {
       this.destinos = [];
  }
  add(d: destinoViaje){
    this.destinos.push(d);
  }
  getAll(): destinoViaje[]{
    return this.destinos;
  }
  getById(id: String):destinoViaje{
    return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
  }

  elegir(d: destinoViaje){
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }

  subscribeOnChange(fn){
  this.current.subscribe(fn);
  }
} 