import {v4 as uuid} from 'uuid';

export class destinoViaje {
  selected: boolean;
  servicios: string[];
  id = uuid();
  constructor(public nombre: string, public imagenUrl: string) {
    this.servicios = ['Almuerzo', 'Desayuno'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }
}
